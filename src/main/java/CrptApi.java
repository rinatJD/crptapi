import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.net.http.HttpClient;

public class CrptApi {
    private final Lock lock = new ReentrantLock();
    private final Condition requestAvailable = lock.newCondition();
    private final int requestLimit;
    private final long requestInterval;
    private int currentRequests = 0;
    private final HttpClient httpClient;;
    private final ObjectMapper objectMapper;

    public CrptApi(TimeUnit timeUnit, int requestLimit) {
        this.requestLimit = requestLimit;
        this.requestInterval = timeUnit.toMillis(1);
        this.httpClient = HttpClient.newHttpClient();
        this.objectMapper = new ObjectMapper();

        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(this::resetRequestCount, requestInterval, requestInterval, TimeUnit.MILLISECONDS);
    }

    private void resetRequestCount() {
        lock.lock();
        try {
            currentRequests = 0;
            requestAvailable.signalAll();
        } finally {
            lock.unlock();
        }
    }

    public void createDocument(Document document, String signature) {
        lock.lock();
        try {
            while (currentRequests >= requestLimit) {
                requestAvailable.await();
            }

            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create("https://ismp.crpt.ru/api/v3/lk/documents/create"))
                    .header("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(document)))
                    .build();

            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

            currentRequests++;
        } catch (IOException | InterruptedException e ) {
            throw new RuntimeException(e);
        } finally {
            lock.unlock();
        }
    }

    static class Description {
        private String participantInn;
        public Description(String participantInn) {
            this.participantInn = participantInn;
        }
        public String getParticipantInn() {
            return participantInn;
        }
    }

    @Getter
    @Setter
    static class Product{
        private String certificate_document;
        private String certificate_document_date;
        private String certificate_document_number;
        private String owner_inn;
        private String producer_inn;
        private String production_date;
        private String tnved_code;
        private String uit_code;
        private String uitu_code;
        private String reg_date;
        private String reg_number;
    }

    @Builder
    @Getter
    @Setter
    static class Document {

        private Description description;
        private String doc_id;
        private String doc_status;
        private String doc_type;
        private Boolean importRequest;
        private String owner_inn;
        private String participant_inn;
        private String producer_inn;
        private String production_date;
        private String production_type;
        private List<Product> products;
        private String reg_date;
        private String reg_number;

    }

    public static void main(String[] args) {

        CrptApi crptApi = new CrptApi(TimeUnit.SECONDS, 3);

        Description description = new Description("string");
        Product product = new Product();
        product.setCertificate_document("string");
        product.setCertificate_document_date("2020-01-23");
        product.setCertificate_document_number("");
        product.setOwner_inn("string");
        product.setProducer_inn("string");
        product.setProduction_date("2020-01-23");
        product.setTnved_code("string");
        product.setUit_code("string");
        product.setUitu_code("string");

        List<Product> products = new ArrayList<>();
        products.add(product);

        Document document = Document.builder()
                .description(description)
                .doc_id("string")
                .doc_status("string")
                .doc_type("LP_INTRODUCE_GOODS")
                .importRequest(true)
                .owner_inn("string")
                .participant_inn("string")
                .producer_inn("string")
                .production_date("2020-01-23")
                .production_type("string")
                .products(products)
                .reg_date("2020-01-23")
                .reg_number("string")
                .build();

        String signature = "signature";

        for (int i = 0; i < 10; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    crptApi.createDocument(document, signature);
                }
            });
            thread.start();
        }
    }
}
